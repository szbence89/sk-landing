$(document).ready(function() {

    var lang = document.documentElement.lang;

    // TAB CONTROL
    $( ".js-tab-control .tab-handle" ).on( "click", function() {
        var tab_control_name = $(this).closest('.js-tab-control').attr('id');
        var target_string = $(this).data("target");
        var target = $('#'+target_string);
        var tab_contents = $(this).closest('.js-tab-control').find(".tab-content");
        var tab_handles = $(this).closest('.tabs').find(".tab-handle");
        tab_contents.removeClass('is-active');
        target.addClass('is-active');
        tab_handles.removeClass('is-active');
        $( this ).closest('li').addClass('is-active');
        
    });
    // TAB CONTROL END
  
    // GENERATE RECONMENDATION CARDS

    var apiURLbase = 'https://api.sportkartya.hu/api/v2/recommendations/sectors?lang=';
    var apiURL = apiURLbase.concat(lang);
    const img_base_url = 'https://img.sportkartya.hu/unsafe/300x150/smart/https://api2.sportkartya.hu';
    const place_base_url = 'https://sportkartya.hu/elfogadohely/';
    const container = $('#recommendations');
    $.getJSON( apiURL, function(jd) {
        $.each(jd.recommendation, function(index, place){
            var card = $('<div><a href="'+place_base_url+place.slug+'" target="_blank" class="card facility-card"><div class="card-image"><figure class="image is-2by1"><img src="'+img_base_url+place.background_image_url+'"><span class="category">'+place.name+'</span></figure></div><div class="card-content"><h1 class="card-title">'+place.site_name+'</h1><p class="address">'+place.address+'</p><div class="card-plans"><span class="icon sk-icon-alpha"></span><span class="icon sk-icon-beta"></span><span class="icon sk-icon-delta"></span><span class="icon sk-icon-omega"></span><span class="icon sk-icon-omega-plus"></span></div></div></a></div>');
            container.append(card);
            $(card).find('.icon').each(function(i) {
                if ( i+1 < place.min_plan_group_id ) {
                   $(this).addClass('is-disabled');
                }
            });

        });
        container.owlCarousel({
              items: 5,
              nav: true,
              navText: ['<i class="fas fa-arrow-left"></i>','<i class="fas fa-arrow-right"></i>'],
              loop: false,
              margin: 30,
              responsiveClass: true,
              responsive: {
                0: {
                  items: 1,
                  nav: false,
                  loop: true
                },
                540: {
                  items: 2,
                  nav: false,
                  loop: true,
                  center: false
                },
                870: {
                  items: 3,
                  nav: true,
                  loop: true
                },
                1024: {
                  items: 3,
                  nav: true,
                  loop: true
                },
                1150: {
                  items: 3,
                  nav: true,
                  loop: true
                }                
              },
              stagePadding: 20,
              center: true,
              autoplay: true,
              autoplayHoverPause: true
        });        
    });

    $('#customer-carousel').owlCarousel({
      items: 1,
      loop: false,
      margin: 30,
      center: true,
      autoplay: true,
      autoplayHoverPause: true
    });   



    // CHECKDOMAIN
    if (lang == 'hu') {
        var check_msg_email_req = "Add meg az e-mail címed az ellenőrzéshez!";
        var check_msg_email_email = "Ellenőrizd az e-mail cím formátumát!";
    } else {
        var check_msg_email_req = "Please enter your name!";
        var check_msg_email_email = "Please check the email address format!";
    }    
    $("#check_domain_form").validate({
        onkeyup: function(element) { $(element).valid(); },
        onfocusout: function(element) { $(element).valid(); },
      rules: {
        email: {
              required: true,
              email: true,
              normalizer: function(value) {
                return $.trim(value);
              }
        }
      },
      messages: {
        email: {
          required: check_msg_email_req,
          email: check_msg_email_email
        }
      },
      onfocusout: true,
      onkeyup: false,
      errorClass: "text-danger form-error",
      submitHandler: function(form) {
        $('#check_domain_button').prop('disabled', true);
        $('#check_domain_msg').html('');
        $.post($("#check_domain_form").attr('action'), { email: form.email.value, lang: form.locale.value })
          .done(function( data ) {
            $('#check_domain_msg').removeClass('is-warning');
            $('#check_domain_msg').removeClass('is-success');
            if(data.available === true){
              $('#check_domain_msg').addClass('is-success');
            }
            else{
              $('#check_domain_msg').addClass('is-warning');
            }
            $('#check_domain_msg').html(data.message);
            $('#check_domain_button').prop('disabled', false);
        });
      }
    });


    // CONTACT
    if (lang == 'hu') {
        var contact_msg_name_req = "Add meg a neved!";
        var contact_msg_email_req = "Add meg az e-mail címed!";
        var contact_msg_email_email = "Ellenőrizd az e-mail cím formátumát!";
        var contact_msg_accept_req = "Az érdeklődés elküldéséhez el kell fogadnod!";
    } else {
        var contact_msg_name_req = "Please enter your name!";
        var contact_msg_email_req = "Please enter your email address!";
        var contact_msg_email_email = "Please check the email address format!";
        var contact_msg_accept_req = "You have to accept the terms!";
    }
    
    $("#contact_form").validate({
        rules: {
          name: {
                required: true,
                normalizer: function(value) {
                  return $.trim(value);
                }
          },
          email: {
                required: true,
                email: true,
                normalizer: function(value) {
                  return $.trim(value);
                }
          },
          accept: {
                required: true,
          },
        },
        messages: {
          name: {
            required: contact_msg_name_req,
          },
          email: {
            required: contact_msg_email_req,
            email: contact_msg_email_email
          },
          accept: {
            required: contact_msg_accept_req,
          }
        },
        onfocusout: true,
        onkeyup: false,
        errorClass: "text-danger form-error",
        errorPlacement: function(error, element) {
            error.appendTo( element.closest(".control").find(".validation_msg"));
        },
        submitHandler: function(form) {
          $('#subscribe_button').prop('disabled', true);
          $('#subscribe_msg').html('');
          $.post($("#contact_form").attr('action'), {
            interest_type: 'newsletter',
            full_name: form.name.value,
            mobile: form.phone.value,
            name: form.name.value,
            email: form.email.value,
            plan_id: form.plan_id.value,
            lang: form.locale.value
          })
            .done(function( data ) {
              $('#subscribe_msg').removeClass('is-success');
              $('#subscribe_msg').html(data.message);
              $('#subscribe_msg').addClass('is-success');
              $('#subscribe_button').prop('disabled', false);
          });
        }
    });

});